let dataset = [1,2,3];
d3.select('body')
  .selectAll('p')
  .data(dataset)
  .enter()
  .append('p')
  .text('D3 demo').style('color', 'red')
  .text(function(d) { return d;}); // displaying data item inside dataset